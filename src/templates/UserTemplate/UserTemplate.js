import React from 'react'

export default function UserTemplate({ Component }) {
  return (
    <div>
      <div style={{ backgroundImage: `url("./img/login-image.jpeg")` }}
        className='w-screen h-screen fixed object-cover flex items-center justify-center -z-10'>
        <div className='absolute w-full h-full bg-white/20'></div>
        <div className='w-[600px] bg-white text-black shadow-xl z-10 rounded-md p-4 laptop:p-12'>
          <img src="./img/logo.png" className='hidden tablet:block w-[200px] h-[60px] object-cover mb-4 mx-auto' alt="Lỗi hình ảnh" />
          <Component />
          <p className='mt-5 max-w-[400px] mx-auto text-xs tablet:text-sm font-light text-center'>Việc bạn tiếp tục sử dụng trang web này đồng nghĩa bạn đồng ý với điều khoản sử dụng của chúng tôi</p>
        </div>
      </div>
    </div>
  )
}
