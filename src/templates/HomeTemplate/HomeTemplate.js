import React, { useEffect } from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'

export default function HomeTemplate({ Component }) {
    useEffect(() => {
        window.scrollTo({ top: 0, behavior: "smooth" })
    })
    return (
        <div className=' bg-white text-black dark:bg-[#081b27] dark:text-white'>
            <Header />
            <Component />
            <Footer />
        </div>
    )
}
