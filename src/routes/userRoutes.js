import Login from "../pages/Login/Login";
import Register from "../pages/Register/Register"
import UserTemplate from "../templates/UserTemplate/UserTemplate";

export const userRoutes = [
    {
        url: "/login",
        component: <UserTemplate Component={Login} />
    },
    {
        url: "/register",
        component: <UserTemplate Component={Register} />
    },
]