import Detail from "../pages/Detail/Detail";
import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login";
import Register from "../pages/Register/Register";
import HomeTemplate from "../templates/HomeTemplate/HomeTemplate";

export const homeRoutes = [
    {
        url: '/',
        component: <HomeTemplate Component={Home}/>
    },
    {
        url: '/detail/:id',
        component: <HomeTemplate Component={Detail}/>
    },
]