import React from 'react'
import { Button, Form, Input } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { fetchApiUserLogin } from '../../toolkit/userSlice';
import { localUser } from '../../utils/localStorage';


export default function Login() {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const onFinish = (values) => {
    // khi đăng nhập thành công thì mới run function này
    const onSuccess = () => {
      // lưu thông tin user xuống localstorage
      localUser.set(values)
      // chuyển hướng vào trang home
      navigate("/")
    }
    // gọi api userLogin khi nhấn submit
    dispatch(fetchApiUserLogin(values, onSuccess))
  };

  return (
    <div className='w-full'>
      <h2 className='mb-4 tablet:mb-8 text-center font-bold text-lg tablet:text-2xl'>Đăng nhập vào movie booking</h2>
      <Form
        name="basic"
        style={{
          maxWidth: 600,
        }}
        validateTrigger="onSubmit"
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Tài khoản"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tài khoản!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="matKhau"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập mật khảu!',
            },
            {
              min: 3,
              message: "Mật khẩu ít nhất 3 ký tự"
            }
          ]}
        >
          <Input.Password />
        </Form.Item>


        <Form.Item className='text-center'>
          <Button type="primary" className='bg-[#1677ff] mt-4 font-semibold' htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
      <h2 className='text-center'>Bạn chưa có tài khoản? <NavLink className="text-red-500 font-semibold" to="/register">Đăng ký</NavLink></h2>

    </div>
  )
}
