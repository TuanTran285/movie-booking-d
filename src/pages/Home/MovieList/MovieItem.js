import { Card } from 'antd'
import Meta from 'antd/es/card/Meta'
import moment from 'moment/moment'
import React from 'react'
import { NavLink } from 'react-router-dom'

export default function MovieItem({ movie }) {
  return (
    <div className='relative col-span-1'>
    <NavLink to={`/detail/${movie.maPhim}`}>
      <Card className='relative w-full aspect-[4/6] overflow-hidden group'
        hoverable
        cover={<img className='aspect-[4/6] w-full min-h-[200px] object-cover transition-all duration-300 hover:scale-110 hover ' alt={movie.hinhAnh} src={movie.hinhAnh} />}
      >
        <div className='group-hover:scale-100 scale-0 transition-all duration-300 absolute top-1/2 left-1/2 text-red-500 -translate-x-1/2 -translate-y-1/2 text-lg'></div>
      {/* <div className='text-white absolute bottom-0 left-1/2 -translate-x-1/2 w-full'>
        <NavLink className="p-4 block bg-red-500">Mua vé</NavLink>
      </div> */}
      </Card>
    </NavLink>
      <Meta className='text-center'
        title={<h4 className='text-yellow-500 font-medium text-[10px] tablet:font-semibold tablet:text-lg'>{movie.tenPhim}</h4>}
        description={<p className='text-back font-medium text-[10px] tablet:text-sm'>{moment(movie.ngayKhoiChieu).format("DD-MM-YYYY")}</p>} />
    </div>
  )
}
