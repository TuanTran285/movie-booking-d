import React, { useEffect, useState } from 'react'
import { movieService } from '../../../services/movieService'
import MovieItem from './MovieItem'
import { Pagination, message } from 'antd'
import "./Movie.css"

export default function MovieList() {
  const [movieList, setMovieList] = useState({})

  const fetchApiMovieList = async (page) => {
    try {
      const result = await movieService.getMovieList(page)
      setMovieList(result.data.content)

    }catch(err) {
        message.error("Api movie page error")
    }
  }
  useEffect(() => {
    fetchApiMovieList(1)
  }, [])

  return (
    <div id='#lichchieu' className='py-10 mb-20'>
      <h2 className='container font-bold text-2xl text-[#06d6a0] text-center tablet:text-4xl'>The Best Films</h2>
      <div className='my-10 container grid grid-cols-3 gap-4 laptop:grid-cols-4 laptop:gap-8
      '>
        {movieList?.items?.map((movie) => {
          return <MovieItem key={movie.maPhim} movie={movie} />
        })}
      </div>
      <Pagination className='container text-right' total={movieList?.totalCount} onChange={(page) => fetchApiMovieList(page)} />
    </div>
  )
}
