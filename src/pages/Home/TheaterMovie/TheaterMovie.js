import React, { useEffect, useState } from 'react'
import { theaterService } from '../../../services/theaterService'
import { Tabs, message } from 'antd';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../toolkit/loadingSlice';
import { SUCCESS } from '../../../utils/setting';

export default function TheaterMovie() {
    const [theaterMovie, setTheaterMovie] = useState([])
    const dispatch = useDispatch()
    const fetchApiTheaterByMovie = async () => {
        try {
            const result = await theaterService.getMovieByTheater()
            if(result.status === SUCCESS) {
                setTheaterMovie(result.data.content)
            }
        }catch(err) {
            message.error(err)
        }
        
    }
    useEffect(() => {
        fetchApiTheaterByMovie()
    }, [])

    const renderTheaterbyMovie = () => {
        return theaterMovie.map(htr => {
            return {
                key: htr.maHeThongRap,
                label: <img className='w-14 h-14 object-cover' src={htr.logo} alt={htr.logo} />,
                children: <Tabs className='h-[500px] text-black dark:text-white' defaultActiveKey="1" tabPosition='left' items={htr.lstCumRap?.map(cumRap => {
                    return {
                        key: cumRap.tenCumRap,
                        label: <div>
                            <h3>{cumRap.tenCumRap}</h3>
                        </div>,
                        children: (
                            <div className='h-[500px] overflow-y-scroll'>
                                {
                                    cumRap.danhSachPhim?.map(phim => {
                                        return <div key={phim.maPhim}>
                                            <div className='flex'>
                                                <img className='w-20 h-28 mb-4 object-cover rounded-md' src={phim.hinhAnh} alt={phim.hinhAnh} />
                                                <div className='ml-4'>
                                                    <h3 className='font-medium text-lg'>{phim.tenPhim}</h3>
                                                    <div>
                                                        {phim.lstLichChieuTheoPhim?.map(lichChieu => {
                                                            return <button onClick={() => message.loading("Developing")} key={lichChieu.maLichChieu} className='p-2 mt-2 mr-2 text-white text-base font-medium bg-yellow-500 rounded-md'>{moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY ~ hh:mm")}</button>
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    })
                                }
                            </div>
                        )
                    }
                })} />
            }
        })
    }
    return (
        <div id='#cumrap' className='container hidden tablet:block'>
            <Tabs className='h-[500px]' defaultActiveKey="1" tabPosition='left' items={renderTheaterbyMovie()} />
        </div> 
    )
}