import React, { useEffect } from 'react'
import CarouselSlider from './CarouselSlider/CarouselSlider'
import MovieList from './MovieList/MovieList'
import TheaterMovie from './TheaterMovie/TheaterMovie'

export default function Home() {
  return (
    <div>
     <CarouselSlider/>
     <MovieList/>
     <TheaterMovie/>
    </div>
  )
}
