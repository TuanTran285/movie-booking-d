import React, { useEffect, useRef, useState } from 'react'
import { SUCCESS, height_Header } from '../../../utils/setting'
import { Button, Carousel, message } from 'antd';
import { movieService } from '../../../services/movieService';
import { setLoadingOn, setLoadingOff } from '../../../toolkit/loadingSlice'
import { useDispatch } from 'react-redux';
export default function CarouselSlider() {
  const [bannerList, setBannerList] = useState([])
  const dispatch = useDispatch()
  const fetchApiBanner = async () => {
    try {
      const result = await movieService.getListBanner()
      if (result.status === SUCCESS) {
        setBannerList(result.data.content)
        setTimeout(() => {
          dispatch(setLoadingOff())
        }, 1000)
      }
    } catch (err) {
      message.error("lấy danh sách banner thất bại")
      dispatch(setLoadingOff())
    }
  }
  useEffect(() => {
    fetchApiBanner()
    dispatch(setLoadingOn())
  }, [])
  return (
    <div id='#phimhot'>
      <Carousel autoplay={true} easing='linear'>
        {bannerList.map((banner) => (
          <div className='w-screen aspect-[4/1.65]' key={banner.maBanner}>
            <img className='w-full h-full object-cover' src={banner.hinhAnh} alt={banner.hinhAnh} />
          </div>
        ))}
      </Carousel>
    </div>
  )
}
