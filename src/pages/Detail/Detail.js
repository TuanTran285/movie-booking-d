import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieService } from '../../services/movieService'
import moment from 'moment'
import { Tabs, message } from 'antd';
import { theaterService } from '../../services/theaterService';
import { SUCCESS } from '../../utils/setting';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../toolkit/loadingSlice';

export default function Detail() {
  const [movieDetail, setmovieDetail] = useState({})
  let { id } = useParams()
  const dispatch = useDispatch()
  const fetchApiShowtimeMovie = async () => {
    try {
      const result = await theaterService.getInfoShowTimeMovie(id)
      if(result.status === SUCCESS) {
        setmovieDetail(result.data.content)
        setTimeout(() => {
          dispatch(setLoadingOff())
        }, 1000)
      }
    }catch(err) {
        message.error(err)
        dispatch(setLoadingOff())
    }
  }
  useEffect(() => {
    fetchApiShowtimeMovie()
    dispatch(setLoadingOn())
  }, [])
  const renderShowtimeMovie = () => {
    return movieDetail.heThongRapChieu?.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-8 h-8 tablet:w-14 tablet:h-14 object-cover" alt="Lỗi" />,
        children: heThongRap.cumRapChieu?.map((rap, index) => {
          return <div key={index} className="mb-4 text-black dark:text-white">
            <div className='flex mt-4 items-center'>
              <img className='w-14 h-14 object-cover rounded-md' src={rap.hinhAnh} alt="Lỗi" />
              <h4 className='ml-2'>{rap.tenCumRap}</h4>
            </div>
            <div className='flex flex-wrap mt-2'>
              {rap.lichChieuPhim?.slice(0, 10).map((lichChieu, index) => {
                return <div className='m-2 ml-0 mr-4 tablet:m-4 tablet:ml-0 tablet:mr-8' key={index}>
                  <button onClick={() => {
                    message.loading("Developing")
                  }} className='bg-slate-500 rounded-md text-xs tablet:text-base laptop:text-lg text-white p-1 laptop:p-2'>
                    {moment(lichChieu.ngayChieuGioChieu).format('DD-MM-YYYY ~ HH:mm')}
                  </button>
                </div>
              })}
            </div>
          </div>
        })
      }
    })
  }
  return (
    <div>
      <div className='container pt-10'>
        <div className='laptop:flex'>
          <img className='w-11/12 tablet:w-2/3 mx-auto h-auto laptop:w-[380px] laptop:h-[550px] object-cover rounded-md' src={movieDetail.hinhAnh} alt={movieDetail.hinhAnh} />
          <div className='w-11/12 tablet:w-2/3 mx-auto laptop:ml-5 flex flex-col justify-center'>
            <h3 className='mt-4 text-xl font-medium text-yellow-500 tablet:text-4xl tablet:leading-[50px] laptop:mt-0'>{movieDetail.tenPhim}</h3>
            <div className='tablet:leading-[45px] tablet:text-2xl'><span className='font-medium text-yellow-500 tablet:text-3xl'>Tổng quan: </span> {movieDetail.moTa}</div>
            <div className='tablet:leading-[45px] tablet:text-2xl'><span className='font-medium text-yellow-500 tablet:text-3xl'>ngày khởi chiếu: </span>{moment(movieDetail.ngayKhoiChieu).format("DD-MM-YYYY hh:mm")}</div>
            <div className='tablet:leading-[45px] tablet:text-2xl'><span className='font-medium text-yellow-500 tablet:text-3xl'>Đánh giá: </span>{movieDetail.danhGia}<i className="las la-star"></i></div>
          </div>
        </div>
        <div className='mt-10'>
          <iframe className='w-full h-[380px] tablet:h-[440px] laptop:h-[580px]' src={movieDetail.trailer}></iframe>
        </div>
        <div>
          <Tabs className='text-white mt-14' tabPosition='left' defaultActiveKey="1" items={renderShowtimeMovie()} />
        </div>
      </div>
    </div>
  )
}
