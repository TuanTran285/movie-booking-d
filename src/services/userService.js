import { https } from "./config"

class UserService {
    userLogin = (userInfo) => {
        return https.post('/api/QuanLyNguoiDung/DangNhap', userInfo)
    }
    userRegister = (newUser) => {
        return https.post('/api/QuanLyNguoiDung/DangKy', newUser)
    }
}

export const userService = new UserService()