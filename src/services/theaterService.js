import { https } from "./config"

class TheaterService {
    getInfoShowTimeMovie = (id) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`)
    }
    getMovieByTheater = () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap")
    }
}

export const theaterService = new TheaterService()