import { https } from "./config"

class MovieService {
    getListBanner = () => {
        return https.get("/api/QuanLyPhim/LayDanhSachBanner")
    }
    getMovieList = (page) => {
        return https.get(`api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=GP11&soTrang=${page.toString()}&soPhanTuTrenTrang=8`)
    }
    getMovieDetail = (id) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`)
    }
}

export const movieService = new MovieService()