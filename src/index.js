import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './toolkit/configureStore';
import { homeRoutes } from './routes/homeRoutes';
import NotFound from './pages/NotFound/NotFound';
import HomeTemplate from './templates/HomeTemplate/HomeTemplate';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import { userRoutes } from './routes/userRoutes';
import Loading from './components/Loading/Loading';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>
    <Provider store={store}>
      <BrowserRouter>
        <Loading />
        <Routes>
          {homeRoutes.map(({ url, component }) => {
            return <Route key={url} path={url} element={component} />
          })}
          {userRoutes.map(({ url, component }) => {
            return <Route key={url} path={url} element={component} />
          })}
          <Route path='*' element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  </div>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
