import React from 'react'
import { useSelector } from 'react-redux'
import { RingLoader } from 'react-spinners'

export default function Loading() {
    const { isLoading } = useSelector(state => state.loadingSlice)
    return isLoading ?  <div className=' h-screen w-screen fixed top-0 left-0 flex justify-center items-center z-50 bg-black/70'>
    <RingLoader color='#36d7b7' /></div> : ""
}
