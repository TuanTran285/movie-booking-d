import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Dropdown, Space, message } from 'antd';

export default function UserMenu({userInfo, handleLogout}) {


  return (
    <div>
      {userInfo ? <Dropdown
        lineWidthFocus="400px"
        menu={{
          items: [
            {
              label: <div className='flex items-center'>
                <img className='rounded-full w-[40px] h-[40px]' src="https://files.fullstack.edu.vn/f8-prod/user_photos/181070/622d3feceac17.jpg" alt="lỗi" />
                <h4 className='ml-4 font-bold'>{userInfo.taiKhoan}</h4>
              </div>,
              key: "0"
            },
            {
              label: <div onClick={() => {message.loading("Developing")}}>Trang cá nhân</div>,
              key: '1',
            },
            {
              label: <NavLink onClick={handleLogout} to="/">Đăng xuất</NavLink>,
              key: '2',
            },
          ],
          style: { width: 200 }
        }}
        trigger={['click']}
      >
        <a onClick={(e) => e.preventDefault()}>
          <Space>
            <img className='rounded-full w-[40px] h-[40px]' src='https://files.fullstack.edu.vn/f8-prod/user_photos/181070/622d3feceac17.jpg' alt="lỗi" />
          </Space>
        </a>
      </Dropdown> : <div><NavLink to="/login" className="mr-4 hover:text-[#ff006e] font-bold">Đăng nhập</NavLink> <NavLink to="/register" className="hover:text-[#ff006e] font-bold">Đăng ký</NavLink></div>}
    </div>
  )
}
