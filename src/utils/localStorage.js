import { USER_INFO } from "./setting"

class LocalUser {
    // lấy giá trị từ localstoage lên
    get = () => {
        let dataJson = localStorage.getItem(USER_INFO)
        return JSON.parse(dataJson)

    }
    // thiết lập giá trị xuống localstorage
    set = (userInfo) => {
        let dataJson = JSON.stringify(userInfo)
        localStorage.setItem(USER_INFO, dataJson)
    }
        
    // xóa giá tri trong localstorage
    remove = () => {
        localStorage.removeItem(USER_INFO)
    }
}

export const localUser = new LocalUser()