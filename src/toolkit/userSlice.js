import { createSlice } from '@reduxjs/toolkit'
import { userService } from '../services/userService';
import { SUCCESS } from '../utils/setting';
import { message } from 'antd';
import { localUser } from '../utils/localStorage';

const initialState = {
    userInfo: localUser.get(),
}

const userSlice = createSlice({
    name: "userSlice",
    initialState,
    reducers: {
        userLogin: (state, action) => {
            state.userInfo = action.payload
        },
    }
});

export const { userLogin } = userSlice.actions

export default userSlice.reducer



// -------redux thunk---------

export const fetchApiUserLogin = (newUser, onSuccess) => {
    return async dispatch => {
        try {
            const result = await userService.userLogin(newUser)
            if (result.status === SUCCESS) {
                dispatch(userLogin(result.data.content))
                onSuccess()
                message.success("Đăng nhập thành công")
            }
        } catch (err) {
            console.log("err", err)
            message.error("Đăng nhập thất bại")
        }
    }
}


export const fetchApiUserRegister = (userInfo, onSuccess) => {
    return async dispatch => {
        try {
            const result = await userService.userRegister(userInfo)
            if (result.status === SUCCESS) {
                onSuccess()
                message.success("Đăng ký tài khoản thành công")
            }
        } catch (err) {
            message.success("Đăng ký tài khoản thất bại")
        }
    }
}