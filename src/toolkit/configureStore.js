import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./userSlice"
import loadingSlice from "./loadingSlice"
const store = configureStore({
    reducer: {
        userSlice,
        loadingSlice
    }
})

export default store